<?php

namespace App\Http\Controllers;

class TownController extends Controller
{
    public function index(){
        return view('pages/home');
    }

    public function profile(){
        return view('pages/profile');
    }

    public function ajaxRequestPost(Request $request)
    {
        $input = $request->all();
        // dd($input);
        return response()->json(['success'=>'Got Simple Ajax Request.']);
    }
}
