<?php

namespace App\Console\Commands;

use App\Jobs\TownsImport;
use Illuminate\Console\Command;
// use Illuminate\Queue\Jobs\Job;
use Sunra\PhpSimple\HtmlDomParser;

class DataImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is used to import data from e-obce.sk. The data is scraped from the web, geocoding data is added and finally it is stored in the DB.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Return dom elements from web.
     *
     * @return string
     */
    function gib_content_pls($http_link){
        $c = \curl_init();
        \curl_setopt($c, CURLOPT_RETURNTRANSFER, TRUE);
        \curl_setopt($c, CURLOPT_URL, $http_link);
        $content = \curl_exec($c);
        \curl_close($c);

        return $content;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Scrape Web, create job for each single town.

        $content = $this->gib_content_pls(env('IMPORT_SOURCE', 'https://www.e-obce.sk/zoznam_vsetkych_obci.html'));

        $dom = HtmlDomParser::str_get_html( $content ); //as file_get_html is not recommended and does not work in my PHP72 on LAMP inside WSL

        foreach($dom->find('a.web') as $elem){
            // create a queue for each elem (site)
            // TODO: validation
            $subsite = $this->gib_content_pls( $elem->href );

            $towns = HtmlDomParser::str_get_html( $subsite );

            foreach($towns->find('a') as $single){
                // because the web is without any reasonable classes, id or whatever identifiers
                // validation
                if(\strpos($single->href, 'www.e-obce.sk/obec/') != FALSE){
                    TownsImport::dispatch($single->href);
                }
            }
        }
    }
}
