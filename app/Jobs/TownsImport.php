<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Sunra\PhpSimple\HtmlDomParser;
use App\Town;

// use function App\Console\Commands\DataImport\gib_content_pls;
# TODO place function somewhere more within reason, like a HelperService Provide or smthg.

class TownsImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $http_link;

    /**
     * Return dom elements from web.
     *
     * @return string
     */
    function gib_content_pls($http_link){
        $c = \curl_init();
        \curl_setopt($c, CURLOPT_RETURNTRANSFER, TRUE);
        \curl_setopt($c, CURLOPT_URL, $http_link);
        $content = \curl_exec($c);
        \curl_close($c);

        return $content;
    }

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($http_link)
    {
        $this->http_link = $http_link;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        # add duplicity check (index?) make a fresh migration? Yeah.
        $dom = HtmlDomParser::str_get_html($this->gib_content_pls($this->http_link));

        $info = array(2);
        $inner = $dom->find("#telo", 0)->first_child()->first_child()->children(1)->first_child()->first_child()->children(2);

        $info[0] = $inner->children(4);
        $info[1] = $inner->children(2);

        # do I trust that here is no injection on e-obce? Yes I do! I would not dare to do this on a production though.
        # most probably I would do it with a different library, or, even, language.
        $mayor = trim($info[0]->children(7)->children(1)->plaintext);
        $phone = trim($info[1]->children(2)->children(3)->plaintext);
        $fax = trim($info[1]->children(3)->children(2)->plaintext);
        $address1 = trim($info[1]->children(4)->children(0)->plaintext);
        $address2 = trim($info[1]->children(5)->children(0)->plaintext);
        $name = trim($info[1]->children(0)->children(0)->plaintext);
        $mail = trim($info[1]->children(4)->children(2)->plaintext);

        $data = [
            'name' => $name,
            'address' => $address1.', '. $address2,
            'mayor' => $mayor,
            'phone' => $phone,
            'fax' => $fax,
            'email' => $mail, #only one per sub-page it would seem
            'web' => $dom->find('[target=newwindow]')[0]->href, #2 such links, looking for the first one
            'img' => $dom->find('[src^=https://www.e-obce.sk/erb/]')[0]->src, #such link is exacly once per sub-page
            'coordinates' => 'fakeData',
        ];

        # get coordinates

        $town = new Town();
        $town::create($data);

    }
}
