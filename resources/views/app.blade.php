<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{-- jQuery --}}
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    {{-- token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}" />
<style>
    .name-home{
        font-size: 45px;
        color: #fff;
    }
    .footer{
        background: #dfdfdf;
        padding-top: 40px;
        color: #545454;
    }
    .nine-h{
        font-size: 9px;
    }
    .list-mute{
        list-style: none;
        font-size: 10px;
    }
    .body-home{
        height: 435px;
        background: linear-gradient(0deg, #11a0f5, #0978f0);
    }
    .body-profile{

    }
    .rapid-alert{
        color: #1065f7;
    }

    .top-search::placeholder{
        background-image: url( {{ asset('img/search.png') }} );
        background-repeat: no-repeat;
        background-position: 135px;
    }
</style>
</head>
<body>
    <div id="app">
        <div class="container">
            <div class="top row">
                <div class="col-12 row navbar navbar-expand-lg" style="margin-top: 10px">
                    {{-- logo search bar contact language bar login button --}}
                    <div class="col-3">
                        <img src={{ asset('img/logo.png') }} alt="IMG">
                    </div>
                    <div class="col-9 navbar collapse navbar-collapse justify-content-end">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link font-weight-bold" href="#">Kontakty a čísla na oddelenia</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" style="color: #aaa6a6" id="langDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    EN
                                </a>
                                <div class="dropdown-menu" aria-labelledby="langDropdown">
                                    <a class="dropdown-item" href="#">SK</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">EN</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <form class="form-inline my-2 my-lg-0">
                                    <input class="form-control mr-sm-2 top-search" style="border-color: #aaa6a6" type="search" placeholder=" " aria-label="Search">
                                </form>
                            </li>
                            <li class="nav-item">
                                <button type="button" class="btn btn-light" style="color: #fff; background: #07d3a9;">Prihlásenie</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="bottom row text-decoration-none">
                <nav class="col-12 nav justify-content-left">
                    <a style="color: black;" class="nav-link" href="#">O nás</a>
                    <a style="color: black;" class="nav-link" href="#">Zoznam miest</a>
                    <a style="color: black;" class="nav-link" href="#">Inšpekcia</a>
                    <a style="color: black;" class="nav-link" href="#">Kontakt</a>
                </nav>
            </div>
        </div>
        <div class="body-home">
            <div class="container">
                @yield('body')
            </div>
        </div>
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-3">
                        <ul class="list-mute">
                            <li class="nine-h font-weight-bold text-uppercase">Adresa a kontakt</li>
                            <li>ŠÚKL</li>
                            <li>Kvetná 11</li>
                            <li>825 08 Bratislava 25</li>
                            <li>Ústredňa:</li>
                            <li>+421-2-50701 111</li>
                        </ul>
                        <ul class="list-mute">
                            <li class="font-weight-bold text-uppercase">Kotakty</li>
                            <li>adresa</li>
                            <li>telefonne čísla</li>
                            <li>úradné hodiny</li>
                            <li>bankové spustenie</li>
                        </ul>
                        <ul class="list-mute">
                            <li class="font-weight-bold text-uppercase">Informácie pre verejnosť</li>
                            <li>Zoznam vyvezených liekov</li>
                            <li>MZ SR</li>
                            <li>Národný portál zdravia</li>
                        </ul>
                    </div>
                    <div class="col-3">
                        <ul class="list-mute">
                            <li class="font-weight-bold text-uppercase">O nás</li>
                            <li>Dotazníky</li>
                            <li>Hlavní predstavitelia</li>
                            <li>Základné dokumenty</li>
                            <li>Zmluvy za ŠÚKL</li>
                            <li>História a súčastnosť</li>
                            <li>Národná spolupráca</li>
                            <li>Medzinárodná spolupráca</li>
                            <li>Poradné orgány</li>
                            <li>Legislatíva</li>
                            <li>Priestupky a správne {{--delikatesy :D--}} delikty</li>
                            <li>Zoznam dĺžnikov</li>
                            <li>Sadzobník ŠÚKL</li>
                            <li>Verejné obstarávanie</li>
                            <li>Vzdeláacie akcie a prezentácie</li>
                            <li>Konzultácie</li>
                            <li>Voľné pracovné miesta (0)</li>
                            <li>Poskytovanie informácií</li>
                            <li>Sťažnosti a petície</li>
                        </ul>
                    </div>
                    <div class="col-3">
                        <ul class="list-mute">
                            <li class="font-weight-bold text-uppercase">Média</li>
                            <li>Tlačové správy</li>
                            <li>Likey v médiach</li>
                            <li>Kontakt pre média</li>
                        </ul>
                        <ul class="list-mute">
                            <li class="font-weight-bold text-uppercase">Databázy a servis</li>
                            <li>Databáza liekov a zdavotníckych pomôcok</li>
                            <li>Iné zoznamy</li>
                            <li>Kontaktný formulár</li>
                            <li>Mapa stránok</li>
                            <li>A - Z index</li>
                            <li>Linky</li>
                            <li>RSS</li>
                            <li>Doplnok pre internetovy prehliadač</li>
                            <li>Prehliadače formátov</li>
                        </ul>
                    </div>
                    <div class="col-3">
                        <ul class="list-mute">
                            <li class="font-weight-bold text-uppercase">Drogové prekurzory</li>
                            <li>Aktuality</li>
                            <li>Legislatíva</li>
                            <li>Pokyny</li>
                            <li>Kontakt</li>
                        </ul>
                        <ul class="list-mute">
                            <li class="font-weight-bold text-uppercase">Iné</li>
                            <li>Linky</li>
                            <li>Mapa stránok</li>
                            <li>FAQ</li>
                            <li>Podmienky používania</li>
                        </ul>
                        <ul class="list-mute rapid-alert">
                            <li class="font-weight-bold text-uppercase">Rapid alert system</li>
                            <li><u>Rýchla výstraha vyplývajúca z nedostatkov v kvalite liekov</u></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

/* @yield('ajax') */

<script type="text/javascript">

$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".shadow-lg").on("input", function(){
        var key = $(".shadow-lg").val();
        console.log(key);

        $.ajax({
        type:'POST',
        url:'/ajaxRequest',
        data:{data: key},
        success:function(data){
            alert(data.success);
        }
        });

    });
});
</script>

</html>
