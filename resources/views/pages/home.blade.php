@extends('app')

@section('body')
<div class="row justify-content-center" style="padding-top:144px">
    <div class="col-6 justify-content-center">
        <div class="name" class="col-12">
            <div class="name-home text-center">Vyhľadať v databáze obcí</div>
        </div>

        <form>
            <input class="form-control form-control-lg shadow-lg" type="search" placeholder="Zadajte názov">
        </form>
    </div>
</div>
@endsection
